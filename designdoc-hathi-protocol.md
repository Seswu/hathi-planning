# Structure

## Title and People
The title of your design doc, the author(s) (should be the same as the list of people planning to work on this project), the reviewer(s) of the doc (we’ll talk more about that in the Process section below), and the date this document was last updated.

## Overview
A high level summary that every engineer at the company should understand and use to decide if it’s useful for them to read the rest of the doc. It should be 3 paragraphs max.

## Context
A description of the problem at hand, why this project is necessary, what people need to know to assess this project, and how it fits into the technical strategy, product strategy, or the team’s quarterly goals.

## Goals and Non-Goals
### Goals
- describe the user-driven impact of your project
  - user might be another engineering team
  - user might even be another technical system
- specify how to measure success using metrics
  - bonus points if you can link to a dashboard that tracks those metrics

### Non-Goals
Non-Goals are equally important - they describe which problems you won’t be fixing so everyone is on the same page.

### Milestones
A list of measurable checkpoints, so your PM and your manager’s manager can skim it and know roughly when different parts of the project will be done.

#### Recent Changes
Anything affecting the planned milestones that they are not currently adjusted for.

### Existing Solution
Description of currently-known solutions, and a high level example flow to illustrate how users interact with this system and/or how data flow through it.

Additional user stories could be added here as examples.

### Proposed Solution - AKA "Technical Architecture Section"
Provide a big picture first, then fill in lots of details. Aim for a world where you can write this, then take a vacation on some deserted island, and another engineer on the team can just read it and implement the solution as you described.

- Try to walk through a user story to concretize this.
- Include many sub-sections and diagrams.

### Alternative Solutions
- What else did you consider when coming up with the solution above?
- What are the pros and cons of the alternatives?
- Have you considered buying a 3rd-party solution — or using an open source one — that solves this problem as opposed to building your own?

### Testability, Monitoring and Alerting
I like including this section, because people often treat this as an afterthought or skip it all together, and it almost always comes back to bite them later when things break and they have no idea how or why.

### Cross-Team Impact
How will this increase on call and dev-ops burden? 
How much money will it cost? 
Does it cause any latency regression to the system? 
Does it expose any security vulnerabilities? 
What are some negative consequences and side effects? 
How might the support team communicate this to the customers?

### Open Questions - AKA "Known Unknowns"
Any open issues that you aren’t sure about, contentious decisions that you’d like readers to weigh in on, suggested future work, and so on.

- Any topic reaching five posts or more are more efficiently solved at a meeting

#### Topic A
- Key argument
  - Counterargument
    - Reference
  - Proof of key argument

#### Topic B
- Wild claim
  - Preposterous technical argument
  - Statement of doubt concerning the ancestry of previous poster

### Detailed Scoping and Timeline
Breakdown of how and when you plan on executing each part of the project.
How large are the tasks at hand? How complicated? How long will they take?
Do they need to be broken down further?

- Ongoing project task tracker
  - update this whenever scoping estimates change
