Architecture of the Hathi Router

The hathi router must accept connections from clients and route messages to
the modules associated with a given message, then return the result to the
client. The router contains the control logic that generates internal messages
and does error handling.

For example when the router recieves a 'get-object' request from a client it
executes the following sequence:

* Check the packet for a 'session' field

* If 'session' present send a 'check-session' request to the IDM

* If check-session is false send an authorization error to the client and exit

* Relay 'get-object' to the OS

* Return the OS's result to the client

(see router-relay.md for the full set of commands)

## On startup:

* Open connections to OS and IDM, wrap them in endpoints, and start a pair of
managers to run them.

* Start a listener for client connections

## Managers

There are two managers, one to handle each of the connections to the other
modules in the server. The reason for having custom management code instead
of using the bare EndpointKernel is because packets need to be sent from
multiple clients. The manager deals with applying request IDs to each packet,
and then when the response arrives from the module it returns the result to
the correct client handler.

Each manager has:

* a channel for recieving packets. This is not used directly by the client
handlers, but through a function call.

* the communication endpoint for the module this manager is talking to.

* a map containing requestIDs associated with client handler channels. Used
to send replies back to the handlers.

The manager is expected to have some sort of SendPacket() function, taking
the packet and a channel for the response.

## Listener

The listener is just a standard server listening loop. Upon recieving a
connection from a client it wraps the connection in an endpoint, creates
a channel, and starts a client handler in a goroutine.

## Client handler

The client handler gets packets and calls the relevant packet handler for
that packet type.

## Packet handler

Packet handlers deal with sending messages to the other modules, getting
and processing results, and returning a response to the client.
