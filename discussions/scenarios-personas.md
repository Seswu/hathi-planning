Scenarios and Personas
-----------------------

1. Internal development
   - Discussion of Nier Automata  
     We should be able to cover all conceivable angles of gameplay
     without being confused 3 months hence.
   - Direct messages  
     So that we can organize The Resistance.
2. Relatives by appointment
   - Manual creation of Aunt Tillie as user
   - User friendly generation and storage of user key pair
   - User creation of recipient lists
   - Mass messaging of knitting recipes
   - Recovery of key pair by email
3. No bad apples
   - Uncle Blackhat might claim to be Aunt Tillie, but he will never
     reach her secret vault with the apple pie recipe.
