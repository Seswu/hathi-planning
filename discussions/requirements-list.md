Project Requirements list
=======================================

```
Authour/Person to blame: UW

Document history: 
18 Nov 2018:
Creation

03 Dec 2018:
Split scope discussion of individual phases off into separate document
Removed 'Like' functionalities. They do not support our use case and could be more generally implemented through taxonomies anyway
```

Overview
--------

### Concept
Hathi will provide a global network of discussion groups, easily searchable, with functionality to quickly give a good overview of in-depth, long-running and complicated technical discussions, free to join by anyone who wishes, revealing as little or as much about themselves as they wish to, and requiring as little hardware and bandwidth as possible.

### Priorities:
- Anonymity  
  The function of the network should be entirely independent of any and all sharing of personal information by users.
- Decentralization  
  To ensure that the user base will never be monopolized or walled in, every component must be designed with the setup of alternative networks (groups of Hathi servers and components) in mind.


Functionality
--------------


### Create a profile

A user should be able to create an account on a Hathi instance.  
The user should be expected to provide the following information:

* Some form of authentication credential for future use such as a username and password.
* A display name

The Hathi instance should store the salted hash of the password with the username and used to validate the user at a later time.  
The instance should also have a mechanism to verify the identity of the user creating the account. The following options are to be considered:

* Send an email with a confirmation code, to be entered by the user to the Hathi instance
* OAuth
* GPG key

Once the Hathi instance has verified the user's identity, the user will be able to login to the Hathi instance using the correct credentials; and the Hathi instance will provide other functionality available to an existing user account.

### Login to Hathi

A user should be able to enter the authentication credential provided when creating the account (see "Create an account" above) to login to their account on the Hathi instance.  
When the credential has been validated, the user is notified and the user is able to access functionality available to authenticated users.

### Update display name

An authenticated user should be able to update the display name of their account.  
The user enters the new value through an interface and submits the new value.  
The Hathi instance should store that information and use that for cases where the user's display name is needed.

### Update their login password

An authenticated user should be able to update their password.  
The user should provide their existing password and the new password.  
When the Hathi instance has validated that the provided existing password is correct it should replace the currently-stored salted hash with a salted hash of the new password.  

At this point, the user should be expected to use the new password.

### View an instance user's profile

Any user should be able to view another user's profile information.

### Request to follow a user

An authenticated user should be able to follow another user.

If the user to follow is on the same instance, the user should be able to send the follow request while viewing the Hathi user's profile.  
If the user to follow is on another ActivityPub instance, the user should be able to enter the other person's ID.

### Disposition a follow request

An authenticated user should be able to accept or reject  another user's follow request.

### Block a user

An authenticated user should be able to block another user either on the same or another instance, as specified in.  

If the user to block is on the same Hathi instance, the user should be able to block using the other user's local ID.

### View followers

An authenticated user should be able to view their followers list.  
From that list, the user should be able to navigate and get more details for anyone on that list.

### View who they are following

An authenticated user should be able to view their following list.  
From that list, the user should be able to navigate and get more details for anyone on that list.

### Sending a message

An authenticated user should be able to create and send a message.  
The user should be able to decide the note's recepient.

If any of the recipients are in another ActivityPub instance, the Hathi network should propagate the relevant messages to the recipients' instances for delivery there.

### Get a list of sent messages

An authenticated user should be able to retrieve a list of messages that they have sent.

### Edit a message

An authenticated user should be able to edit a message that they have previously created.
(But already-sent messages cannot be retroactively altered)

### Get a list of received messages

An authenticated user should be able to retrieve a list of messages they have received.  
These messages can be sent explicitly to them; or have been produced due to public posts created by a user they are following.

### View a message

A user should be able to view messages that they have been given access to by the message's creator.  
An authenticated user should also be able to view a message that they created on the Hathi instance.

If the message is in reply to another message, a reference to that message should be displayed.  

Similarly, if the message has replies, a reference to the replies should be displayed.

### Reply to a message

An authenticated user should be able to reply to a message that they are able to view.  
When this happens, the Hathi instance should include a reference to the ID of the message being replied to.

### View a list of messages in a hierarchical thread format

A user should be able to view the list of messages in a thread view.



User Interfaces
----------------

All interfaces will contain the following menu:

- Messages
  - Compose message
	- Send message
	- Cancel message
  - View message
    - Close message
  - Reply to message
    - Send message
    - Cancel message
- Recipient Lists
  - Add list
  - View list
    - Close list
  - Edit list
    - Add recipient
    - Remove recipient
    - Close list
  - Delete list


All interfaces will contain the following displays:

- Login
  By completion of which it is possible to access the remaining functionality.
- Menu  
  Lists a number of choices and does things when the choices are chosen.
- List  
  Lists a number of things and presents relevant things such as topics
  in a readable form.
  Also sender and date because we like to hold people accountable.
- Discussion view  
  A hierarchical, chronological presentation of messages.

### Text Interface

Interface should be minimal, and provide for keyboard shortcuts
throughout.  
Displays will take one screen each, and only one display will be
active at any one time.  
Minimalism. Just like smartphones.

### Graphical Desktop Interface

Interface should focus on providing a good default overview.  
There are two likely use cases for this interface: "Forum" and "Chat".  
[Open Issue] How do we want to play those?

Example: Composite View  
Server|Group|Topic list to the left, Messages view to the right.  
![Composite View](assets/UI-desktop_groups-topics-msgview_layout.png)

### Web Interface
[Keith: Description]
